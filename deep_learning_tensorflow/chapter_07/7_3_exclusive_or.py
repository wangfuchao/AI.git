import tensorflow as tf
import numpy as np

learning_rate = 1e-4
n_input = 2   # 输出层节点数
n_label = 1
n_hidden = 2  # 隐藏层节点数

x = tf.placeholder(tf.float32, [None, n_input])
y = tf.placeholder(tf.float32, [None, n_label])

# 定义学习参数
weights = {
    'h1': tf.Variable(tf.truncated_normal([n_input, n_hidden], stddev=0.1)),
    'h2': tf.Variable(tf.random_normal([n_hidden, n_label], stddev=0.1))
}
biases = {
    'h1': tf.Variable(tf.zeros([n_hidden])),
    'h2': tf.Variable(tf.zeros([n_label]))
}

# 定义网络模型
layer_1 = tf.nn.relu(tf.add(tf.matmul(x, weights['h1']), biases['h1']))
# y_pred = tf.nn.tanh(tf.add(tf.matmul(layer_1, weights['h2']), biases['h2']))
# y_pred = tf.nn.relu(tf.add(tf.matmul(layer_1, weights['h2']), biases['h2']))
# y_pred = tf.nn.sigmoid(tf.add(tf.matmul(layer_1, weights['h2']), biases['h2']))

# leaky relus
layer_2 = tf.add(tf.matmul(layer_1, weights['h2']), biases['h2'])
y_pred = tf.maximum(layer_2, 0.01*layer_2)

loss = tf.reduce_mean((y_pred - y)**2)
train_step = tf.train.AdamOptimizer(learning_rate).minimize(loss)

# 构建模拟数据
x1 = [[0, 0], [0, 1], [1, 0], [1, 1]]
y1 = [[0], [1], [1], [0]]
tx = np.array(x1).astype('float32')
ty = np.array(y1).astype('int16')

# 启动 Session
sess = tf.InteractiveSession()
sess.run(tf.global_variables_initializer())

# 训练
for i in range(10000):
    sess.run(train_step, feed_dict={x: tx, y: ty})

# 计算预测值
print('计算预测值: ', sess.run(y_pred, feed_dict={x: tx}))

# 查看隐藏层的输出
print('查看隐藏层的输出: ', sess.run(layer_1, feed_dict={x: tx}))



