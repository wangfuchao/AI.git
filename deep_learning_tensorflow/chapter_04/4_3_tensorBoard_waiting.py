import tensorflow as tf

with tf.name_scope('tensor0'):
    tensor0 = tf.constant(3, tf.float32)
with tf.variable_scope('tensor1'):
    tensor1 = tf.Variable(tf.random_uniform([1]))

init = tf.global_variables_initializer()
add_tensor = tf.add(tensor0, tensor1)

with tf.Session() as sess:
    sess.run(init)
    writer = tf.summary.FileWriter('logs/test0', sess.graph)
    print(sess.run(add_tensor))
    writer.close()

