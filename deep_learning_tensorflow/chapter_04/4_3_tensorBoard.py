import tensorflow as tf
import numpy as np

# 构建实验数据
train_x = np.linspace(-1, 1, 100)
# y = 2 * x + b
train_y = 2. * train_x + np.random.randn(*train_x.shape) * 0.3

# 创建模型
# 占位符

X = tf.placeholder(tf.float32)
Y = tf.placeholder(tf.float32)
# 模型参数
weights = tf.Variable(tf.random_normal([1]), name='weights')
biases = tf.Variable(tf.zeros([1]), name='biases')
z = tf.multiply(X, weights) + biases
tf.summary.histogram('z', z)  # 将预测之以直方图的形式显示  直方图名字是 'z'

# 构建损失函数

loss = tf.reduce_mean(tf.square(Y - z))
tf.summary.scalar('loss_func', loss)  # 将损失以标量形式显示 标量名字叫 loss_func
# 定义学习率
learning_rate = 0.01
tf.summary.scalar('LR', learning_rate)
# 构建优化函数
optimizer = tf.train.GradientDescentOptimizer(learning_rate)
# 最小化损失函数
train = optimizer.minimize(loss)

# 初始化所有变量
init = tf.global_variables_initializer()

# 定义 epochs
training_epochs = 20
# 每隔两步显示一次中间值
display_step = 2

# 启动Session
with tf.Session() as sess:
    # 初始化全局变量
    sess.run(init)
    merged_summary_op = tf.summary.merge_all()  # 合并所有 summary
    # 创建 summary_writer 用于写文件
    summary_writer = tf.summary.FileWriter('logs/mnist_summaries', graph=sess.graph)

    # 向模型中 feed 数据
    for epoch in range(training_epochs):
        for (x, y) in zip(train_x, train_y):
            feed_dict = {X: x, Y: y}
            sess.run(train, feed_dict=feed_dict)

        # 生成summary
        summary_str = sess.run(merged_summary_op, feed_dict=feed_dict)
        summary_writer.add_summary(summary_str, epoch)

        # 显示训练中的数据
        if epoch % display_step == 0:
            loss_ = sess.run(loss, feed_dict={X: train_x, Y: train_y})
            print('epoch:', epoch + 1, 'loss = ', loss_, 'weights=',
                  sess.run(weights), 'biases=', sess.run(biases))
            # 保存检查点
    print('Finished...')
    # 保存模型
    print('loss=', sess.run(loss, feed_dict={X: train_x, Y: train_y}), 'weights=',
          sess.run(weights), 'biases=', sess.run(biases))
