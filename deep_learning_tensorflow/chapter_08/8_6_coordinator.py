import tensorflow as tf

'''
实验演示 协调器的用法

首先建立一个 100 大小的队列。
主线程使用计数器不停地 +1，队列线程再把主线程里的计数器放到队列中。
当队列为空时，主线程在sess.run(queue.dequeue())语句挂起，
当队列线程写入队列中时，主线程的计数器同步开始工作，
整个操作都是在with语句中执行的，由于使用了Coordinator，
当session要关闭之前会进行 coord.request_stop 函数
将所有的线程关闭，之后才会关闭session
'''
queue = tf.FIFOQueue(100, 'float')    # 创建大小为100的队列
c = tf.Variable(0.)
op = tf.assign_add(c, tf.constant(1.))  # +1 操作
enqueue_op = queue.enqueue(c)   # 将计数器的结果加入队列

'''
创建一个队列管理器QueueRunner，用这两个操作向 q 中添加元素。目前我们只使用一个线程
'''
qr = tf.train.QueueRunner(queue, enqueue_ops=[op, enqueue_op])

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())

    coord = tf.train.Coordinator()

    # 启动入队线程， Coordinator 是线程的参数
    enqueue_threads = qr.create_threads(sess, coord=coord, start=True)  # 启动入队线程

    for i in range(10):   # 主线程
        print('------------------------')
        print(sess.run(queue.dequeue()))

    # 通知其他线程关闭，其他所有线程关闭之后，这一函数才能返回
    coord.request_stop()

    # 还可以用 coord.join(enqueue_threads) 指定等待某个进程结束


