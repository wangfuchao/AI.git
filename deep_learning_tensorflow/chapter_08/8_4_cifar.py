import tensorflow as tf
import pylab

'''
从 https://github.com/tensorflow/models.git 下载models，TensorFlow1.0 之后就把 Models 模块分离了出来。
然后从models/tutorials/image中把cifar10文件夹复制到工作区
'''

from cifar10 import cifar10_input
data_dir = './cifar/cifar-10-batches-bin'   # 下面的路径请按照自己数据放的路径写

batch_size = 128   # 一次取数据的大小


'''
程序默认的是使用测试数据集，如果使用训练数据集，需要 eval_data=False
'''
images_test, labels_test = cifar10_input.inputs(eval_data=True, data_dir=data_dir, batch_size=batch_size)

'''
以下面的方式启动Session，为什么这里不使用with语法？？？
因为 with 语法是自动关闭Session的。运行结束后Session自动关闭的同时会把里面的所有操作都关掉，
而此时队列还在等待另一个进程往里面写数据，此时就会出现错误。(下面测试)
可以改为下面这种方式：
sess = tf.Session()
tf.global_variables_initializer().run(session=sess)
tf.train.start_queue_runners(sess=sess)
这样写在单例程序中没问题，资源会随着程序的关闭而整体销毁，但是在复杂的代码中，需要某个线程自动关闭，
而不是依赖进程的结束而销毁，这种情况下需要使用 tf.train.Coordinator 函数来创建一个协调器
以信号量的方式来协调线程间的关系，完成线程间的同步
'''
sess = tf.InteractiveSession()
tf.global_variables_initializer().run()   # 初始化全局参数

'''
TensorFlow中提供了一个队列机制，通过多线程将读取数据与计算数据分开。
因为在处理海量数据集的训练时，无法把数据集一次全部载入到内存中，需要边读边训练

tf.train.start_queue_runners()  这句话的作用是启动线程，向队列中读取数据
删除这句话会导致程序处于一个挂起的状态，因为 image_batch, label_batch = sess.run([images_test, labels_test])
这句代码是从队列中拿出指定批次的数据。但是队列中没有数据，所以程序进入挂起等待状态
'''
tf.train.start_queue_runners()
image_batch, label_batch = sess.run([images_test, labels_test])

# print('image_batch[0]： \n', image_batch[0])
# print('label_batch[0]： \n', label_batch[0])
pylab.imshow(image_batch[0])
pylab.show()


# '''
# 出错代码
# '''
# with tf.Session() as sess:
#     tf.global_variables_initializer().run()  # 初始化全局参数
#     tf.train.start_queue_runners()
#     image_batch, label_batch = sess.run([images_test, labels_test])
#
#     pylab.imshow(image_batch[0])
#     pylab.show()


'''
要使用 with语句 加上协调器
'''
with tf.Session() as sess:
    tf.global_variables_initializer().run()  # 初始化全局参数
    coord = tf.train.Coordinator()
    tf.train.start_queue_runners(sess, coord)
    image_batch, label_batch = sess.run([images_test, labels_test])

    pylab.imshow(image_batch[0])
    pylab.show()

    coord.request_stop()
