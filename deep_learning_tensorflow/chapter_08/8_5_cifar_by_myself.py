import numpy as np
import pylab
file_name = './cifar/cifar-10-batches-bin/test_batch.bin'
byte_stream = open(file_name, 'rb')
buffer = byte_stream.read(10000*(1+32*32*3))
byte_stream.close()

data = np.frombuffer(buffer, dtype=np.uint8)
data = data.reshape(10000, 1+32*32*3)   # reshape 后 第一列为标签列
print('data: ', data[:, 0:1])

labels_images = np.hsplit(data, [1])        # 将标签和图像分割开  [1] 表示第一列为一个分组，其余的为一个分组
print('labels_images: ', labels_images[0])
print('labels_images: ', labels_images[1][20])

labels = labels_images[0].reshape(10000)
print('labels: ', len(labels))
images = labels_images[1].reshape(10000, 32, 32, 3)
print('images: ', images.shape)
print('images[0]: ', images[0])
img = np.reshape(images[0], (3, 32, 32))
print('img: ', img)
img = img.transpose(1, 2, 0)
print('img0: ', img)


print(labels[0])
pylab.imshow(img)
pylab.show()

# data:  [[3] [8] [8] ... [5] [1] [7]]
# labels_images:  [[3] [8] [8] ... [5] [1] [7]]
# labels_images:  [ 48  65  84 ... 112 115 126]
# labels:  10000
# images:  (10000, 32, 32, 3)
# 3
