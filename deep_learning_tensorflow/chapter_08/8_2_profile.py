import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import matplotlib.image as im_img

my_img = im_img.imread('./static/yaya.jpeg')

# plt.imshow(my_img)
# plt.axis('off')      # 不显示坐标轴
# plt.show()
# print(my_img.shape)  # 输出图片的大小及通道数

# 定义占位符、卷积核、卷积op
full = np.reshape(my_img, [1, 1633, 1200, 3])
input_full = tf.Variable(tf.constant(1., shape=[1, 1633, 1200, 3]))
sobel = tf.Variable(tf.constant([[-1., -1., -1.], [0, 0, 0], [1., 1., 1.],
                                 [-2., -2., -2.], [0, 0, 0], [2., 2., 2.],
                                 [-1., -1., -1.], [0, 0, 0], [1., 1., 1.]
                                ], shape=[3, 3, 3, 1]))

# 3 个通道输入， 生成 1 个feature map
# 卷积的不长 1x1, padding为 SAME 表明这是个同卷积操作
op = tf.nn.conv2d(input_full, sobel, strides=[1, 1, 1, 1], padding='SAME')

'''
sobel算子处理过的图片不保证每个像素都在255之间， 所以要做一次归一化操作，让生成的值都在【0， 1】之间，然后再乘以255
'''
o = tf.cast(((op - tf.reduce_min(op))/(tf.reduce_max(op) - tf.reduce_min(op)))*255, tf.uint8)

# 运行卷积操作并显示

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())

    print(sess.run(sobel))
    t, f = sess.run([o, sobel], feed_dict={ input_full: full })
    t = np.reshape(t, [1633, 1200])

    plt.imshow(t, cmap='Greys_r')
    plt.axis('off')
    plt.show()


# [
#  [
#   [[-1.] [-1.] [-1.]]
#   [[ 0.] [ 0.] [ 0.]]
#   [[ 1.] [ 1.] [ 1.]]
#  ]
#  [
#   [[-2.] [-2.] [-2.]]
#   [[ 0.] [ 0.] [ 0.]]
#   [[ 2.] [ 2.] [ 2.]]
#  ]
#  [
#   [[-1.] [-1.] [-1.]]
#   [[ 0.] [ 0.] [ 0.]]
#   [[ 1.] [ 1.] [ 1.]]
#  ]
# ]




