import tensorflow as tf

'''
定义输入变量
定义一个输入变量用来模拟输入图片、4x4大小的2通道矩阵，并将其赋予指定的值。
2个通道分别为：4个 0 0 0 0到3 3 3 3 组成矩阵，4个 4 4 4 4到7 7 7 7组成的矩阵
'''
img = tf.constant([
    [[0., 4.], [0., 4.], [0., 4.], [0., 4.]],
    [[1., 5.], [1., 5.], [1., 5.], [1., 5.]],
    [[2., 6.], [2., 6.], [2., 6.], [2., 6.]],
    [[3., 7.], [3., 7.], [3., 7.], [3., 7.]]
])
img = tf.reshape(img, [1, 4, 4, 2])

'''
定义池化操作
定义了 4 个池化操作和一个取均值操作
'''
pooling0 = tf.nn.max_pool(img, [1, 2, 2, 1], [1, 2, 2, 1], padding='VALID')
pooling1 = tf.nn.max_pool(img, [1, 2, 2, 1], [1, 1, 1, 1], padding='VALID')
pooling2 = tf.nn.avg_pool(img, [1, 4, 4, 1], [1, 1, 1, 1], padding='SAME')
pooling3 = tf.nn.avg_pool(img, [1, 4, 4, 1], [1, 4, 4, 1], padding='SAME')

nt_hpool2_flat = tf.reshape(tf.transpose(img), [-1, 16])
print('tf.transpose(img): ', tf.transpose(img).shape)
pooling4 = tf.reduce_mean(nt_hpool2_flat, 1)  # 按行求均值

# 运行池化操作
with tf.Session() as sess:
    print('image: ', sess.run(img))
    print('result0: \n', sess.run(pooling0))
    print('result1: \n', sess.run(pooling1))
    print('result2: \n', sess.run(pooling2))
    print('result3: \n', sess.run(pooling3))
    print('result4: \n', sess.run(pooling4))
    print('flat: \n', sess.run(nt_hpool2_flat))


