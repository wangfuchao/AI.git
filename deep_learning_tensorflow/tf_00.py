import numpy as np
import tensorflow as tf

with tf.Session() as sess:
    node1 = tf.constant(3., dtype=tf.float32)
    node2 = tf.constant(4.)
    tensor = sess.run([node1, node2])
    print(node1, node2)
    print(tensor)

print('===============================================================')

sess = tf.Session()
node1 = tf.constant(3., dtype=tf.float32)
node2 = tf.constant(4.)

print(node1, node2)
print(sess.run([node1, node2]))
print('===============================================================')

node3 = tf.add(node1, node2)
print('node3: ', node3)
print('sess.run(node3): ', sess.run(node3))

print('===============================================================')

a = tf.placeholder(tf.float32)
b = tf.placeholder(tf.float32)
add_t = a + b
print(sess.run(add_t, {a: 3., b: 4.}))
print(sess.run(add_t, feed_dict={a: [2., 3.], b: [4., 5.]}))

print('===============================================================')

w = tf.Variable([.3], dtype=tf.float32)
b = tf.Variable([-.3], dtype=tf.float32)

x = tf.placeholder(dtype=tf.float32)
linear_model = w*x+b

init = tf.global_variables_initializer()
sess.run(init)

print(sess.run(linear_model, feed_dict={x: [1, 2, 3, 4]}))

print('111===============================================================')

# loss func
y = tf.placeholder(tf.float32)
deltas = tf.square(linear_model - y)
loss = tf.reduce_sum(deltas)
print('loss func: ', sess.run(loss, {x: [1, 2, 3, 4], y: [0, -1, -2, -3]}))
print('===============================================================')

# ft.assign 变量重新赋值
ficW = tf.assign(w, [-1.])
ficB = tf.assign(b, [1.])
sess.run([ficW, ficB])
print(sess.run(linear_model, {x: [1, 2, 3, 4]}))
print(sess.run(loss, {x: [1, 2, 3, 4], y: [0, -1, -2, -3]}))

print('===============================================================')

optimizer = tf.train.GradientDescentOptimizer(0.01)
train = optimizer.minimize(loss)
sess.run(init)
for i in range(1000):
    sess.run(train, {x: [1, 2, 3, 4], y: [0, -1, -2, -3]})

print(sess.run([w, b]))
print('===============================================================')

# Complete program:The completed trainable linear regression model is shown here:完整的训练线性回归模型代码
# Model parameters
w = tf.Variable([.3], dtype=tf.float32)
b = tf.Variable([-.3], dtype=tf.float32)
# Model input and output
x = tf.placeholder(tf.float32)
linear_model = w * x + b
y = tf.placeholder(tf.float32)

# loss
loss = tf.reduce_sum(tf.square(linear_model - y))  # sum of the squares
# optimizer
optimizer = tf.train.GradientDescentOptimizer(0.01)
train = optimizer.minimize(loss)

# training data
x_train = [1, 2, 3, 4]
y_train = [0, -1, -2, -3]
# training loop
init = tf.global_variables_initializer()
sess = tf.Session()
sess.run(init)  # reset values to wrong
for i in range(1000):
    sess.run(train, {x: x_train, y: y_train})

# evaluate training accuracy
curr_W, curr_b, curr_loss = sess.run([w, b, loss], {x: x_train, y: y_train})
print("W: %s b: %s loss: %s" % (curr_W, curr_b, curr_loss))
print('===============================================================')
